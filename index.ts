import * as pulumi from "@pulumi/pulumi";
import * as digitalocean from "@pulumi/digitalocean";
import * as k8s from "@pulumi/kubernetes";

const name = "demo"

const cluster = new digitalocean.KubernetesCluster(name, {
  region: "lon1",
  version: "1.19.3-do.3",
  nodePool: {
    name: "pool",
    size: "s-1vcpu-2gb",
    nodeCount: 2,
  },
});

export const kubeConfigs = cluster.kubeConfigs;
export const kubeConfigsRaw = cluster.kubeConfigs[0].rawConfig;

const clusterProvider = new k8s.Provider(name, {
  kubeconfig: kubeConfigs[0].rawConfig,
});

const example = new k8s.yaml.ConfigFile("example", {
  file: "manifest.yml",
});

const nginxIngress = new k8s.yaml.ConfigFile("nginxIngress", {
  file: "https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.43.0/deploy/static/provider/do/deploy.yaml",
  transformations: [
    (obj: any, opts: pulumi.CustomResourceOptions) => {
      if (obj.kind === "Deployment" && obj.metadata.name === "ingress-nginx-controller") {
        obj.spec.replicas = 2;
      }
    },
  ]
});
